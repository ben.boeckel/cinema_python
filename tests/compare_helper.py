#==============================================================================
# Copyright (c) 2015,  Kitware Inc., Los Alamos National Laboratory
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this
# list of conditions and the following disclaimer in the documentation and/or other
# materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors may
# be used to endorse or promote products derived from this software without specific
# prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
# OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#==============================================================================
from numpy import linalg as la
from scipy.signal import correlate2d as c2d

import vtk
from vtk.numpy_interface import dataset_adapter as dsa
import numpy


def rgb2grey(image):
    image_lum = image.sum(2) / 3.0
    return image_lum

def normalizeForCC(image):
    image_n = (image - image.mean()) / image.std()
    return image_n

def compare_ncc(image1, image2):
    image1_n = normalizeForCC(rgb2grey(image1))
    image2_n = normalizeForCC(rgb2grey(image2))

    # take the mean in case there are suble differences in size
    return float((c2d(image1_n, image2_n, 'valid') / image1_n.size).mean())

def compare_l2(image1, image2):
    return float(abs(la.norm(image1) - la.norm(image2)))

def vtkRenderToArray(rw, mode="color"):
    # Render & capture image
    rw.Render()

    w2i = vtk.vtkWindowToImageFilter()
    w2i.SetInput(rw)

    if mode is "color":
        w2i.SetInputBufferTypeToRGB()
    elif mode is "Z":
        w2i.SetInputBufferTypeToZBuffer()

    w2i.Update()

    image = w2i.GetOutput()

    # Convert image to a numpy array (flipped to have origin at upper left)
    npview = dsa.WrapDataObject(image)
    idata = npview.PointData[0]
    ext = image.GetExtent()
    width = ext[3] - ext[2] + 1
    height = ext[1] - ext[0] + 1
    if image.GetNumberOfScalarComponents() == 1:
        imageslice = numpy.flipud(idata.reshape(width,height))
    else:
        imageslice = numpy.flipud(idata.reshape(width,height,image.GetNumberOfScalarComponents()))

    return imageslice

def pvRenderToArray(view_proxy):
    image = view_proxy.CaptureWindow(1)
    npview = dsa.WrapDataObject(image)
    idata = npview.PointData[0]
    ext = image.GetExtent()
    width = ext[3] - ext[2] + 1
    height = ext[1] - ext[0] + 1
    imageslice = numpy.flipud(idata.reshape(height, width, 3))

    return imageslice



